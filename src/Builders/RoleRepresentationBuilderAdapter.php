<?php
namespace Scito\Laravel\Keycloak\Admin\Builders;

use Scito\Keycloak\Admin\Representations\RoleRepresentationInterface;

/**
 * Class RoleRepresentationBuilderAdapter
 *
 * @method static RoleRepresentationBuilderAdapter name(?string $username)
 * @method static RoleRepresentationInterface build()
 *
 * @package Keycloak\Admin\Laravel\Builders
 */
class RoleRepresentationBuilderAdapter extends AbstractBuilderAdapter
{

}
