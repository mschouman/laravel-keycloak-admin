# Keycloak Admin API Client for Laravel

Package which adds some wrappers for Laravel on top of the [Keycloak Admin](https://gitlab.com/scito/keycloak-admin)
library.

# Install

Install the package with composer
```
composer require scito/laravel-keycloak-admin
```

Publish the config file

```
php artisan vendor:publish  --provider="Scito\Laravel\Keycloak\Admin\KeycloakServiceProvider"

```

# Adding a user

Using representations:

```php

$user = UserRepresentation
    ::username($username)
    ->password($password)
    ->build();
KeycloakAdmin::users()->add($user);
```

Or using the fluent api:

```php
KeycloakAdmin::users()
    ->create()
    ->username($username)
    ->password($password)
    ->email($email)
    ->save();
);

// Using an options array
KeycloakAdmin::users()
    ->create([
        'username' => $username,
        'password' => $password,
        'email' => $email
    ])
    ->save();
);

```

